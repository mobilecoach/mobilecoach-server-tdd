echo "Starting the docker containers.."
cd development
docker-compose start

cd ../production
docker-compose start

cd ..
docker-compose start
