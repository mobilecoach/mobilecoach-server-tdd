#!/bin/bash

echo "Running this script will remove the changes made in the contents of the folder."
echo "Are you sure you don't have any improtant data in this folder? (Answer with 'yes')"
read -e ANSWER
if [ ! "$ANSWER" = "yes" ]
  then
  echo "Please restart the script to enter the information again."
  exit 0
fi
git clean -fd
git reset --hard


echo "[Development instance] Please enter the hostname of the server (example: workshop-cdhi.ethz.ch):"
read -e domain_development

echo "[Development instance] Please enter the tomcat port (example: 8443):"
read -e tomcat_port_development

echo "[Development instance] Please enter the deepstream port (example: 6020):"
read -e deepstream_port_development

echo "[Production instance] Please enter the hostname of the server (example: workshop-cdhi.ethz.ch):"
read -e domain_production

echo "[Production instance] Please enter the tomcat port (example: 8444):"
read -e tomcat_port_production

echo "[Production instance] Please enter the deepstream port (example: 6021):"
read -e deepstream_port_production

echo "[Production instance] Please enter your email id for letsencrypt notices (example: abcd@ef.gh):"
read -e email

echo "[Development instance] The informations provided are hostname: $domain_development, tomcat port: $tomcat_port_development, deepstream port: $deepstream_port_development and email: $email ."

echo "[Production instance] The informations provided are hostname: $domain_production, tomcat port: $tomcat_port_production, deepstream port: $deepstream_port_production and email: $email ."


echo "Are you sure of these details, and used this script with 'sudo' and want to continue setup? If okay, please answer with 'yes':"
read -e ANSWER

if [ ! "$ANSWER" = "yes" ]
  then
  echo "Please restart the script to enter the information again."
  cd ..
  exit 0
fi

# Letencrypt for development

cd development

certbot --standalone certonly --config-dir letsencrypt/config --work-dir letsencrypt/work --logs-dir letsencrypt/logs --non-interactive --agree-tos -m $email -d $domain_development

keystorepass_development=`openssl rand -hex 8`

echo "Please copy and use $keystorepass_development as export password below."

openssl pkcs12 -export -in ./letsencrypt/config/live/${domain_development}/fullchain.pem -inkey ./letsencrypt/config/live/${domain_development}/privkey.pem -name tomcat -out certs/cert.p12 -CAfile \
 ./letsencrypt/config/live/${domain_development}/chain.pem -caname root

keytool -importkeystore -deststorepass $keystorepass_development -destkeypass $keystorepass_development -destkeystore certs/keystore_tomcat -srckeystore certs/cert.p12 -srcstoretype PKCS12 -srcstorepass $keystorepass_development -alias tomcat

keytool -importkeystore -srckeystore certs/keystore_tomcat -destkeystore certs/keystore_tomcat -deststoretype pkcs12 -srcstorepass $keystorepass_development

# Letencrypt for production

cd ../production

certbot --standalone certonly --config-dir letsencrypt/config --work-dir letsencrypt/work --logs-dir letsencrypt/logs --non-interactive --agree-tos -m $email -d $domain_production

keystorepass_production=`openssl rand -hex 8`

echo "Please copy and use $keystorepass_production as export password below."

openssl pkcs12 -export -in ./letsencrypt/config/live/${domain_production}/fullchain.pem -inkey ./letsencrypt/config/live/${domain_production}/privkey.pem -name tomcat -out certs/cert.p12 -CAfile \
 ./letsencrypt/config/live/${domain_production}/chain.pem -caname root

keytool -importkeystore -deststorepass $keystorepass_production -destkeypass $keystorepass_production -destkeystore certs/keystore_tomcat -srckeystore certs/cert.p12 -srcstoretype PKCS12 -srcstorepass $keystorepass_production -alias tomcat

keytool -importkeystore -srckeystore certs/keystore_tomcat -destkeystore certs/keystore_tomcat -deststoretype pkcs12 -srcstorepass $keystorepass_production

# Renaming... 
cd ..

find . -type f -exec sed -i 's/<hostname_development>/'"$domain_development"'/g' {} +
find . -type f -exec sed -i 's/<port_tomcat_development>/'"$tomcat_port_development"'/g' {} +
find . -type f -exec sed -i 's/<port_deepstream_development>/'"$deepstream_port_development"'/g' {} +
find . -type f -exec sed -i 's/<password_keystore_development>/'"$keystorepass_development"'/g' {} +

find . -type f -exec sed -i 's/<hostname_production>/'"$domain_production"'/g' {} +
find . -type f -exec sed -i 's/<port_tomcat_production>/'"$tomcat_port_production"'/g' {} +
find . -type f -exec sed -i 's/<port_deepstream_production>/'"$deepstream_port_production"'/g' {} +
find . -type f -exec sed -i 's/<password_keystore_production>/'"$keystorepass_production"'/g' {} +


