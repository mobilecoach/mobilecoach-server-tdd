echo "Restarting the docker containers.."
cd development
docker-compose restart

cd ../production
docker-compose restart

cd ..
docker-compose restart
