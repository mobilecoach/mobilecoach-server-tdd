echo "Creating and starting the docker containers.."
cd development
docker-compose up -d

cd ../production
docker-compose up -d

cd ..
docker-compose up -d
